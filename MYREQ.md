# Dealing with MyREQ `Authorization Required` popup

When installing applications on TechNet, you might be hit by this popup:

![](assets/myreq/authorization_required.jpg)

<br>

## To resolve it

1.  Click on the `Click here to raise request via MyREQ` hyperlink from the popup, or navigate from
    the DSO homepage \
    _(see the below section: [`Another way to access MyREQ Software Installation with EPM webpage`](#another-way-to-access-myreq-software-installation-with-epm-webpage))_.

1.  It should bring you to the `Software Installation with EPM` page.

    ![](assets/myreq/form_1.png) <br><br>

1.  Set `Request Type` and `Network` to these:

    ![](assets/myreq/form_2.png) <br><br>

1.  For `Hostname`, open `Command Prompt` on your computer and run
    `I:\Util\Computer\Check_Your_Hostname.bat` path to obtain your TechNet's hostname _(eg.
    `D23007785`)_:

    ![](assets/myreq/hostname_cmd.png) <br><br>

1.  For `Software Title and Version`, manually type out the software's name.

1.  For `Purpose`, you can leave it blank as it's (currently) optional.

1.  For `Enter 8 Digits Challenge code`, copy the 8 digits from the popup:

    > _**NOTE:** The input box only allows 6 characters, so when you copy the popup's code, there's
    > a space `" "` character in the copied 6 digits which you need to remove._

    ![](assets/myreq/form_3.png) <br><br>

1.  Check the acknowledgement checkbox and submit.

1.  You'll be redirected to the below page. Click on the `Request Item` number:

    ![](assets/myreq/response_code_1.png) <br><br>

1.  You should see this, with the 6 digit response code _(eg. `27462438`)_ as highlighted below.

    > If not, try refreshing the page. It usually takes < 10s, so if you don't see that 6 digits
    > after a few mins of refreshing, you might need to contact your supervisor.

    ![](assets/myreq/response_code_2.png) <br><br>

1.  Copy that response code back into the popup, and click `Authorize`:

    ![](assets/myreq/response_code_3.png)

<br>

## Another way to access MyREQ `Software Installation with EPM` webpage

1.  Go to DSO homepage at https://singaporedso.sharepoint.com

1.  Navigate `e-Services` > `D'Lite Apps` > `MyREQ`

    ![](assets/myreq/from_homepage_1.jpg) <br><br>

1.  Click on `Request Something`:

    ![](assets/myreq/from_homepage_2.png) <br><br>

1.  Click on `Software Installation with EPM`:

    ![](assets/myreq/from_homepage_3.png)
