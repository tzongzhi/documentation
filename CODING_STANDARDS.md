# Coding standards

This contains the steps to setup auto-formatters for VSCode.

All the mentioned config files are also given in the [`configs/`](configs) dir.

<br>

## C++

### What these steps does:

-   Sets up C/C++ auto-formatter on VSCode.
-   Configure VSCode to auto-format on save.
-   Configure the format style to use.

### Steps:

1.  Install `C/C++` by Microsoft extension on VSCode.
1.  Open command palette _(ie. `Ctrl+Shift+P`)_.
1.  Search and select `Preferences: Open User Settings (JSON)`, which opens `settings.json`.
1.  Add this to your `settings.json`:
    ```json
    // In `settings.json`:
    {
        // ... other settings ...
        "[cpp][c]": {
            "editor.defaultFormatter": "ms-vscode.cpptools",
            "editor.formatOnSave": true
        },
        "C_Cpp.clang_format_fallbackStyle": "{ BasedOnStyle: Google, IndentWidth: 4, ColumnLimit: 100 }"
    }
    ```

### Setup project's format style

This setups a project to auto-format to the same format, ignoring users' VSCode format settings.

In the project root, create a `.clang-format` file and write this into it:

```yaml
# In `.clang-format` file:
BasedOnStyle: Google
IndentWidth: 4
ColumnLimit: 100
```

<br>

## Javascript/Typescript

### What these steps does:

-   Sets up Prettier auto-formatter on VSCode for JS/TS.
-   Configure VSCode to auto-format on save.
-   Configure the format style to use.

### Steps

1.  Install `Prettier - Code formatter` by Prettier extension on VSCode.
1.  Open command palette _(ie. `Ctrl+Shift+P`)_.
1.  Search and select `Preferences: Open User Settings (JSON)`, which opens `settings.json`.
1.  Add this to your `settings.json`:
    ```json
    // In `settings.json`:
    {
        // ... other settings ...
        "[javascript][typescript][javascriptreact][typescriptreact]": {
            "editor.defaultFormatter": "esbenp.prettier-vscode",
            "editor.formatOnSave": true
        },
        "prettier.trailingComma": "es5",
        "prettier.tabWidth": 4,
        "prettier.semi": false,
        "prettier.singleQuote": true,
        "prettier.jsxSingleQuote": true,
        "prettier.printWidth": 100
    }
    ```

### Setup project's format style

This setups a project to auto-format to the same format, ignoring users' VSCode format settings.

1.  In the project, NPM install the `prettier` and `prettier-plugin-organize-imports` packages:
    ```bash
    # In Bash/Command Prompt:
    npm install --save-dev prettier prettier-plugin-organize-imports
    ```
1.  In the project's root dir, create a `.prettierrc.yaml` file and write this into it:

    ```yaml
    # In `.prettierrc.yaml` file:
    trailingComma: "es5"
    tabWidth: 4
    semi: false
    singleQuote: true
    jsxSingleQuote: true
    printWidth: 100

    # for auto-sorting imports (only works for project prettier configs)
    plugins: ["prettier-plugin-organize-imports"]
    ```
