# Installing CMake on Windows

1.  Download `Windows x64 Installer` from https://cmake.org/download/
1.  When running the installer, if you get an `Authorization Required` popup, refer to the
    [MYREQ.md](MYREQ.md) documentation.
1.  If asked if you want to add CMake to your PATH _(to run in terminals)_, select yes.

<br>

# Building C++ projects

1.  You need a `CMakeLists.txt` file in the project root.

    > Regarding what to write in it, it's too complicated to explain here, so refer to our existing
    > `CMakeLists.txt` or refer to online.

1.  In Bash / Command Prompt, navigate to the directory of the `CMakeLists.txt`

1.  Make a directory named `build` _(or you can give it any other name)_

1.  Navigate into the `build` directory.

1.  Run the command:

    ```bash
    cmake ..         # Generates temp files to be built.
    cmake --build .  # Compiles / Builds ur code.

    # Or in 1 line:
    cmake .. && cmake --build .
    ```

1.  Then run the generated `.exe` file.

    > Typically it'll be in `build/Debug` or `build/bin/Debug` directory, but it depends on the what
    > was set in the `CMakeLists.txt`.

1.  To rebuild, just do the `cmake .. && cmake --build .` \
    It should run much faster on subsequent runs due to caching.

    To remove the CMake cache, just delete the `build` directory.

<br>

Here's a full bash script to compile and run via CMake:

```bash
#!/bin/bash
mkdir -p build
cd build

echo -e "\nGenerating project files via CMake in \"./build\" dir..."
cmake ..

echo -e "\nBuilding via CMake in \"./build\" dir..."
cmake --build .

echo -e "\nRunning compiled executable \"./build/PATH/TO/EXECUTABLE.exe\"..."
./PATH/TO/EXECUTABLE.exe
```

<br>

Save the above bash code into a `.sh` file, and run it via Bash / Git Bash:

```bash
bash SCRIPT.sh
```

<br>

# Setting up CMake debugger for VSCode

## Setup steps

1.  Install VSCode extension `CMake Tools` by Microsoft.

1.  Wait for the CMake icon to appear in the VSCode sidebar:

    ![](assets/cmake/cmake_sidebar_icon.png)

    > _**Note:** If no icon appears after ~2mins, refer to the [Troubleshooting](#troubleshooting)
    > section._

    <br>

1.  **If at any time, a VSCode prompt appears**, refer to the [Possible prompts](#possible-prompts)
    section.

1.  Click on the CMake icon in the sidebar > hover over `Debug` > click on the "play" button:

    ![](assets/cmake/debug_play_button.png)

    > _**Note:** If it throws an error, refer to the [Troubleshooting](#troubleshooting) section._

    <br>

1.  It should compile and run the your executable in debug mode.

    As mentioned before, if a VSCode prompt appears, refer to the
    [Possible prompts](#possible-prompts) section.

<br>

## Possible prompts

### Select a Kit

If VSCode prompts `Select a Kit for [YOUR_REPO_NAME]`, select the `amd64` option _(ie. for x64,
assuming you're building for Windows x64)_.

![](assets/cmake/select_kit.png)

<br>

### Select a launch target

If VSCode prompts `Select a launch target for [YOUR_REPO_NAME]`, select the executable that you want
to debug.

![](assets/cmake/select_launch_target.png)

<br>

### Select CMakeLists.txt

If VSCode prompts `Select CMakeLists.txt`, select the `CMakeLists.txt` of the dir that you want to
debug.

<br>

## Troubleshooting

When doing the setup steps, if there're any problems, just open VSCode's command palette _(via
`Ctrl+Shift+P`)_ and run: \
`Cmake: Reset CMake Tools Extension State (For troubleshooting)`

Some possible problems:

-   Side bar doesn't have the CMake icon:

    ![](assets/cmake/cmake_sidebar_icon.png)<br><br>

-   Running the debug throws an error, for example:

    ![](assets/cmake/debug_error.png)<br><br>
