# Setting up VSCode

1.  Download VSCode x64 installer from online.
1.  When running the installer, if you get an `Authorization Required` popup, refer to the
    [MYREQ.md](MYREQ.md) documentation.

<br>

## Fixing VSCode integrated terminal failing to start

If you tried opening the VSCode integrated terminal _(eg. via `Ctrl+J` or <code>Ctrl+`</code>)_,
it'll likely throw this error:

![](assets/technet_setup/vscode_terminal_error.png)

<br>

This is because powershell is disallowed on the TechNet, and VSCode defaults to powershell.

To fix it:

1.  Open up VSCode's command palette _(via `Ctrl+Shift+P`)_
1.  Search for and select `Preferences: Open User Settings (JSON)`
1.  Add this to the `settings.json` file:
    ```json
    {
        // ... other configs ...
        "terminal.integrated.defaultProfile.windows": "Command Prompt",
        "terminal.integrated.profiles.windows": {
            "PowerShell": null
        }
    }
    ```
1.  This disables the powershell terminal option, and defaults to using command prompt instead. \
    Now you should be able to open your VSCode integrated terminal.

<br>

# Setting up Visual Studio / Visual Studio's MSVC compiler

> _**Note:**_
>
> -   _You **MUST** install the programs in order below, otherwise the Visual Studio installer might
>     throw an error._
> -   _When running the installers, if you get an `Authorization Required` popup, refer to the
>     [MYREQ.md](MYREQ.md) documentation._

1.  Go to https://visualstudio.microsoft.com/downloads
1.  Scroll down to the `All Downloads` section.
1.  Under `Other Tools, Frameworks, and Redistributables` >
    `Microsoft Visual C++ Redistributable for Visual Studio 2022`
    -   Download and install both `x64` and `x86` versions \
        _(without these 2, the VS installer later will fail to install `microsoft.visualcpp.redist.14`)_
1.  Then download either:
    -   If you want the Visual Studio IDE: `Visual Studio` > `Visual Studio Community 2022`
    -   If you just want the compiler: `Tools for Visual Studio` >
        `Build Tools for Visual Studio 2022`
1.  Open up the installer and install `Workloads` > `Desktop development with C++`
